<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class StudentsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {

        return view('students.index');
    }

    public function create()
    {
        return view('students.create');
    }

    public function store()
    {
        $data = request()->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'birth_month' => 'required',
            'birth_day' => 'required',
            'birth_year' => 'required',
        ]);

        auth()->user()->students()->create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'birth_month' => $data['birth_month'],
            'birth_day' => $data['birth_day'],
            'birth_year' => $data['birth_year'],
        ]);

        return redirect('/classroom/' . auth()->user()->id);
    }
}
