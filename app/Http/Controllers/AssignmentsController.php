<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class AssignmentsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {

        return view('assignments.index');
    }

    public function create()
    {
        return view('assignments.create');
    }

    public function store()
    {
        $data = request()->validate([
            'name' => 'required',
            'due_date' => 'required',
            'points_possible' => 'required',
        ]);

        auth()->user()->students()->create([
            'name' => $data['name'],
            'due_date' => $data['due_date'],
            'points_possible' => $data['points_possible'],
        ]);

        return redirect('/classroom/' . auth()->user()->id);
    }
}
