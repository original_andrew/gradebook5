@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-9">
            {{-- this is the $user's name, $user->name --}}
            <h1>{{ Auth::user()->name }}'s class</h1>
            <div>
                <div>Number of Students: <strong>4</strong></div>
                <div>Number of Assignments: <strong>4</strong></div>
            </div>
            <a href="#">Add New Student</a>
        </div>
    </div>
</div>
@endsection
