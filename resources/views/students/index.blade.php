@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-9">
            {{-- this is the $user's name, $user->name --}}
            <h1>Student: {{ Auth::user()->name }}</h1>
            <div>
                <div>Number of Assignments: <strong>4</strong></div>
            </div>
            <a href="#">Add New Student</a>
        </div>
    </div>
    <div class="row pt-5">
        @foreach(Auth::user()->students as $student)
            <div class='card'>
                <h3>Name: {{ $student->first_name . ' ' . $student->last_name }}</h3>
                <h4>Date of Birth: {{ $student->birth_month . "/" . $student->birth_day . "/" . $student->birth_year }}</h4>
            </div>
        @endforeach
    </div>
</div>
@endsection
