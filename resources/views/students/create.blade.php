@extends('layouts.app')

@section('content')
<div class='container'>
    <form action="/students" enctype="multipart/form-data">
        @csrf

        <div class='row'>
            <div class="col-12">
                <div class="form-group row">
                    <label for="first_name" class="col-md-4 col-form-label text-md-right">First Name</label>

                    <div class="col-md-6">
                        <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" required autocomplete="name" autofocus>

                        @error('first_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="last_name" class="col-md-4 col-form-label text-md-right">Last Name</label>

                    <div class="col-md-6">
                        <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}" required autocomplete="name" autofocus>

                        @error('last_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="birth_month" class="col-md-4 col-form-label text-md-right">Birth Month</label>

                    <div class="col-md-6">
                        <input id="birth_month" type="number" class="form-control @error('birth_month') is-invalid @enderror" name="birth_month" value="{{ old('birth_month') }}" required autocomplete="name" autofocus>

                        @error('birth_month')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="birth_day" class="col-md-4 col-form-label text-md-right">Birth Day</label>

                    <div class="col-md-6">
                        <input id="birth_day" type="number" class="form-control @error('birth_day') is-invalid @enderror" name="birth_day" value="{{ old('birth_day') }}" required autocomplete="name" autofocus>

                        @error('birth_day')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="birth_year" class="col-md-4 col-form-label text-md-right">Birth Year</label>

                    <div class="col-md-6">
                        <input id="birth_year" type="number" class="form-control @error('birth_year') is-invalid @enderror" name="birth_year" value="{{ old('birth_year') }}" required autocomplete="name" autofocus>

                        @error('birth_year')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class='form-group row'>
                    <button class='btn btn-primary' type='submit'>Add New Student</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
