@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-9">
            {{-- this is the $user's name, $user->name --}}
            <h1>Assignment: {{ Auth::user()->name }}</h1>
            <div>
                <div>Number of Assignments: <strong>4</strong></div>
            </div>
        </div>
    </div>
</div>
@endsection
